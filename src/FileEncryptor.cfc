component accessors="true" {

    /**
     * @hint cfFileEncryptor Settings Bean
     */
    property name="settingsBean";


    public FileEncryptor function init( Struct config={} ){
        this.setSettingsBean( new Settings( arguments.config ) );

        return this;
    }

    /**
    * Function to upload your file based on a form field passed
    * @fileField This is the field used on your form to upload a file
    * @acceptTypes An array of acceptable mimetypes allowed for this particular file upload. Default is setup in config.
    * @newFileName A different name for the file that will be utilized in the json saved to the file. This name will be used when you reload the file to the browser. DO NOT PASS File Extension. File extension will be appended based on actual file uploaded.
    *
    */
    public String function uploadFile( required String fileField, String acceptTypes = arrayToList( getSettingsBean().getAllowableExtensions() ), String newFileName, numeric maxFileUploadSize = 0, string s3Directory = "" ){
        // upload file to server and save in memory
        var _file = fileUpload( destination="ram://", fileField=arguments.fileField, nameConflict="makeUnique" );
        var _serverFile = "ram://" & _file.serverFile;
        if( arguments.maxFileUploadSize GT 0 AND _file.fileSize GT arguments.maxFileUploadSize )
        {
            fileDelete( _serverFile );
            var _filesizeMsg = "The maximum file upload size is #arguments.maxFileUploadSize/1048576# MB. Please try optimizing the document or lowering the DPI.";
            throw(message="The file size of the uploaded file was not accepted by the server",detail=_filesizeMsg);
        }
        // check file for acceptable mime type
        /**
        NOTE: if the mimetype of a specific file extension is not found, it most likely needs to be added to your server since most applications are not installed on servers
        ColdFusion - <ColdFusion-home>/runtime/lib/mime.types
        Railo - <Railo-home>/tomcat/conf/web.xml
        **/
        var _fileMimeType = getPageContext().getServletContext().getMimeType(_serverFile);
        if( !isDefined("_fileMimeType") || listFindNoCase( arguments.acceptTypes, _fileMimeType ) lte 0 ){
            // Cleanup File Memory - remove from RAM
            fileDelete( _serverFile );

            var _detailMsg = "The file type ";
            if( isDefined("_fileMimeType") ) _detailMsg &= "(" & _fileMimeType & ") ";
            _detailMsg &= "uploaded to the server was not an acceptable MIME type.";

            throw(message="The MIME type of the uploaded file was not accepted by the server",detail=_detailMsg);
        }

        var fileData = {};
        // encrypt file currently stored in memory
        fileData.content = encryptFile( _serverFile );
        fileData.mimeType = _file.contentType & "/" & _file.contentSubType;
        fileData.fileName = _file.clientFile;
        if( structKeyExists( arguments, "newFileName" ) && len( trim( arguments.newFileName ) ) ) fileData.fileName = arguments.newFileName & "." & _file.clientFileExt;

        var newFile = "";
        if( getSettingsBean().getS3Enabled() ) {
            // write encrypted data to s3
            var _fileName = ( len(trim( arguments.s3Directory )) ) ? arguments.s3Directory & "/" : "";
            _fileName &= createUUID();
            var _serializedData = serializeJSON(fileData);

            newFile = s3Upload(fileName=_fileName, contentType=_fileMimeType, fileData=_serializedData, bucket=getSettingsBean().getS3BucketName(), accessKeyId=getSettingsBean().getS3AccessKeyId(), secretKey=getSettingsBean().getS3SecretAccessKey());
        }
        else if ( getSettingsBean().getRackspaceEnabled() ) {
            // write encrypted data to Rackspace Cloud
            var _rackspace = createObject("component", "rackspace").init( getSettingsBean().getRackspaceUsername(), getSettingsBean().getRackspaceApiKey(), getSettingsBean().getRackspaceRegion(), getSettingsBean().getRackspaceBucketName() );
            var _serializedData = serializeJSON(fileData);
            newFile = createUUID();

            if ( _rackspace.authorize() ) {
                _rackspace.uploadFile( newFile, _serializedData );
            }
        }
        else {
            // write encrypted data to file
            newFile = getUniqueFileName();
            fileWrite( newFile, serializeJSON(fileData) );
        }

        // Cleanup File Memory - remove from RAM
        fileDelete( _serverFile );

        return newFile;
    }

    public Struct function readFile( required String filePath ){
        var _file = "";
        if( getSettingsBean().getS3Enabled() ) {
            var _httpService = new http();
            _httpService.setCharset("utf-8");
            _httpService.setMethod( "get" );
            _httpService.setUrl( filePath );

            var _result = _httpService.send().getPrefix();
            if( _result.status_code != 200 ) {
                throw(type="cfFileEncryptor.error.S3FileReadError", detail="#_result.filecontent#", message="#_result.errordetail#");
            }

            _file = DeserializeJSON( _result.fileContent );
        }
        else if ( getSettingsBean().getRackspaceEnabled() ) {
            // read encrypted data from Rackspace Cloud
            var _rackspace = createObject("component", "rackspace").init( getSettingsBean().getRackspaceUsername(), getSettingsBean().getRackspaceApiKey(), getSettingsBean().getRackspaceRegion(), getSettingsBean().getRackspaceBucketName() );

            if ( _rackspace.authorize() ) {
                _file = _rackspace.downloadFile( arguments.filePath );
            }
        }
        else {
            _file = DeserializeJSON( fileRead( arguments.filePath ) );
        }
        _file.content = decryptFile( _file.content );

        return _file;
    }

    private Binary function decryptFile( required String fileContent ){
        try{
            return binaryDecode( arguments.fileContent, getSettingsBean().getBinaryEncoding());
        }catch(Any e){
            throw(type="cfFileEncryptor.error.FileDecryptionError", message=e.detail);
        }
    }

    private String function encryptFile(required String localFile){
        try{
            // encrypt data
            var _bytes = fileReadBinary( arguments.localFile ); // convert file to binary data
            return BinaryEncode( _bytes, getSettingsBean().getBinaryEncoding() );
        }catch(Any e){
            throw(type="cfFileEncryptor.error.FileEncryptionError", message=e.detail);
        }
    }

    private String function getUniqueFileName(){
        var _fileName = this.getSettingsBean().getDirectoryPath() & "/" & createUUID();

        // check to see if file name previously exists
        if( fileExists( _fileName ) ){
            _fileName = getUniqueFileName(); // recursively try until unique file name is found
        }

        return _fileName;
    }

    private String function s3Upload( required fileName, required contentType, required fileData, required bucket, required accessKeyID, required secretKey, acl="public-read", storageClass="STANDARD", httpTimeout="300" ) {
        var _dateTimeString = getHTTPTimeString( now() );
        var _cs = "PUT\n\n#arguments.contentType#\n#_dateTimeString#\nx-amz-acl:#arguments.acl#\nx-amz-storage-class:#arguments.storageClass#\n/#arguments.bucket#/#arguments.fileName#";
        var _signature = createS3Signature(_cs, arguments.secretKey);
        var _url = "http://s3.amazonaws.com/#arguments.bucket#/#arguments.fileName#";

        var _httpService = new http();
        _httpService.setCharset( "utf-8" );
        _httpService.setMethod( "PUT" );
        _httpService.setUrl( _url );

        _httpService.addParam( type="header", name="Authorization", value="AWS #arguments.accessKeyId#:#_signature#");
        _httpService.addParam( type="header", name="Content-Type", value="#arguments.contentType#");
        _httpService.addParam( type="header", name="Date", value="#_dateTimeString#");
        _httpService.addParam( type="header", name="x-amz-acl", value="#arguments.acl#");
        _httpService.addParam( type="header", name="x-amz-storage-class", value="#arguments.storageClass#");
        _httpService.addParam( type="body", value="#arguments.fileData#");

        var _result = _httpService.send().getPrefix();
        if( _result.status_code != 200 ) {
            throw(type="cfFileEncryptor.error.S3FileUploadError", detail="#_result.filecontent#", message="#_result.errordetail#");
        }

        return _url;
    }

    private Binary function HMAC_SHA1( required String signKey, required String signMessage ) {
        var jMsg = javaCast( "string", arguments.signMessage ).getBytes( "iso-8859-1" );
        var jKey = javaCast( "string", arguments.signKey ).getBytes( "iso-8859-1") ;
        var key = createObject( "java", "javax.crypto.spec.SecretKeySpec" );
        var mac = createObject( "java", "javax.crypto.Mac" );

        key = key.init( jKey,"HmacSHA1" );
        mac = mac.getInstance( key.getAlgorithm() );
        mac.init( key );
        mac.update( jMsg );

        return mac.doFinal();
    }

    private String function createS3Signature( required in, required secretKey ) {
        // Replace "\n" with "chr(10) to get a correct digest
        var fixedData = replace(arguments.in,"\n", chr(10), "all");

        // Calculate the hash of the information
        var digest = HMAC_SHA1(arguments.secretKey,fixedData);

        // fix the returned data to be a proper signature
        var signature = toBase64(digest);

        return signature;
    }
}
