/**
* Service Layer for communicating with the Rackspace API
*/
component accessors="true" output="false" {
    property string username;
    property string apiKey;
    property string region;
    property string bucketName;
    property string apiToken;
    property string endPoint;

    public rackspace function init( required string username, required string apiKey, required string region, required string bucketName ){
        setUsername( arguments.username );
        setApiKey( arguments.apiKey );
        setRegion( arguments.region );
        setBucketName( arguments.bucketName );

        return this;
    }

    public boolean function authorize() {
        variables._ret = false;

        variables._postData = {};
        variables._postData["auth"] = {};
        variables._postData["auth"]["RAX-KSKEY:apiKeyCredentials"] = {};
        variables._postData["auth"]["RAX-KSKEY:apiKeyCredentials"]["username"] = getUsername();
        variables._postData["auth"]["RAX-KSKEY:apiKeyCredentials"]["apiKey"] = getApiKey();

        variables._httpClient = new http();
        variables._httpClient.setMethod( 'POST' );
        variables._httpClient.setCharset( 'utf-8' );
        variables._httpClient.setTimeout( 1000 );
        variables._httpClient.setUrl( 'https://identity.api.rackspacecloud.com/v2.0/tokens' );
        variables._httpClient.addParam( type='header', name='Content-Type', value="application/json" );
        variables._httpClient.addParam( type='body', value=serializeJSON( variables._postData ) );
        variables._httpResult = variables._httpClient.send().getPrefix();

        if ( NOT isDefined( "variables._httpResult.statusCode" ) ) {
            throw( type="Rackspace API Error", message="The Rackspace Cloud API server did not respond." );
        }
        else if ( NOT isJSON(variables._httpResult.filecontent) ) {
            throw( type="Rackspace API Error", message="The Rackspace Cloud API server responded with invalid data." );
        }
        else if ( left(variables._httpResult.statusCode, 3) NEQ "200" ) {
            variables._result = deserializeJSON( variables._httpResult.filecontent );
            if ( structKeyExists( variables._result, "badRequest" ) && structKeyExists( variables._result.badRequest, "message" ) ) {
                throw( type="Rackspace API Error", message=variables._result.badRequest.message );
            }
            else {
                throw( type="Rackspace API Error", message="The Rackspace Cloud API server responses with an invalid error message." );
            }
        }
        else {
            variables._result = deserializeJSON( variables._httpResult.filecontent );
            if ( structKeyExists( variables._result, "access" ) &&
                 structKeyExists( variables._result.access, "token" ) &&
                 structKeyExists( variables._result.access.token, "id" ) &&
                 structKeyExists( variables._result.access, "serviceCatalog" ) &&
                 isArray( variables._result.access.serviceCatalog )
                ) {
                setApiToken( variables._result.access.token.id );

                for ( var _service in variables._result.access.serviceCatalog ) {
                    if ( structKeyExists( _service, "name" ) && _service.name == "cloudFiles" ) {
                        for ( var _endpoint in _service.endpoints ) {
                            if ( structKeyExists( _endpoint, "region" ) && _endpoint.region == getRegion() ) {
                                setEndPoint( _endpoint.publicURL );

                                break;
                            }
                        }

                        break;
                    }
                }

                if ( len(trim( getApiToken() )) && len(trim( getEndPoint() )) ) {
                    variables._ret = true;
                }
            }
            else {
                throw( type="Rackspace API Error", message="The Rackspace Cloud API server responses with an invalid token packet." );
            }
        }

        return variables._ret;
    }

    public void function uploadFile( required string fileName, required string fileContent ) {
        variables._httpClient = new http();
        variables._httpClient.setMethod( 'PUT' );
        variables._httpClient.setCharset( 'utf-8' );
        variables._httpClient.setTimeout( 1000 );
        variables._httpClient.setUrl( getEndPoint() & '/' & getBucketName() & '/' & arguments.fileName );
        variables._httpClient.addParam( type='header', name='X-Auth-Token', value=getApiToken() );
        variables._httpClient.addParam( type='header', name='Content-Type', value='text/plain' );
        variables._httpClient.addParam( type='body', value=arguments.fileContent );
        variables._httpResult = variables._httpClient.send().getPrefix();

        if ( NOT isDefined( "variables._httpResult.statusCode" ) ) {
            throw( type="Rackspace API Error", message="The Rackspace Cloud API server did not respond." );
        }
        else if ( ListFindNoCase("200,201", left(variables._httpResult.statusCode, 3)) LTE 0 ) {
            if ( len(trim( variables._httpResult.errordetail )) ) {
                throw( type="Rackspace API Error", message=variables._httpResult.errordetail );
            }
            else {
                throw( type="Rackspace API Error", message="The Rackspace Cloud API server responses with an invalid error message." );
            }
        }
    }

    public struct function downloadFile( required string fileName ) {
        variables._ret = {};
        variables._httpClient = new http();
        variables._httpClient.setMethod( 'GET' );
        variables._httpClient.setCharset( 'utf-8' );
        variables._httpClient.setTimeout( 1000 );
        variables._httpClient.setUrl( getEndPoint() & '/' & getBucketName() & '/' & arguments.fileName );
        variables._httpClient.addParam( type='header', name='X-Auth-Token', value=getApiToken() );
        variables._httpResult = variables._httpClient.send().getPrefix();

        if ( NOT isDefined( "variables._httpResult.statusCode" ) ) {
            throw( type="Rackspace API Error", message="The Rackspace Cloud API server did not respond." );
        }
        else if ( NOT isJSON(variables._httpResult.filecontent) ) {
            try{
                variables._ret = deserializeJSON( variables._httpResult.filecontent ); //attempt to deserialize - have received false negatives
            }catch(any e){
                throw( type="Rackspace API Error", message="The Rackspace Cloud API server responded with invalid data." );
            }
        }
        else if ( left(variables._httpResult.statusCode, 3) NEQ "200" ) {
            if ( len(trim( variables._httpResult.errordetail )) ) {
                throw( type="Rackspace API Error", message=variables._httpResult.errordetail );
            }
            else {
                throw( type="Rackspace API Error", message="The Rackspace Cloud API server responses with an invalid error message." );
            }
        }
        else {
            variables._ret = deserializeJSON( variables._httpResult.filecontent );
        }

        return variables._ret;
    }

    public void function deleteFile( required string fileName ) {
        variables._httpClient = new http();
        variables._httpClient.setMethod( 'DELETE' );
        variables._httpClient.setCharset( 'utf-8' );
        variables._httpClient.setTimeout( 1000 );
        variables._httpClient.setUrl( getEndPoint() & '/' & getBucketName() & '/' & arguments.fileName );
        variables._httpClient.addParam( type='header', name='X-Auth-Token', value=getApiToken() );
        variables._httpResult = variables._httpClient.send().getPrefix();

        if ( NOT isDefined( "variables._httpResult.statusCode" ) ) {
            throw( type="Rackspace API Error", message="The Rackspace Cloud API server did not respond." );
        }
        else if ( left(variables._httpResult.statusCode, 3) NEQ "200" ) {
            if ( len(trim( variables._httpResult.errordetail )) ) {
                throw( type="Rackspace API Error", message=variables._httpResult.errordetail );
            }
            else {
                throw( type="Rackspace API Error", message="The Rackspace Cloud API server responses with an invalid error message." );
            }
        }
    }
}
